from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import urllib.request
from parse_gooogle import *


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

help_message = ("Этот бот поможет тебе узнать погоду.\n" +
                "Примеры запросов: «Владивосток», «погода в Москве», \n" +
                "«какая погода в Сан-Франциско завтра»")

wrong_request_message = ('упс... что-то пошло не так\n' +
                         'это точно запрос о погоде?')
date_words = ['сегодня', 'завтра', 'послезавтра', 'вечер', 'утро', 'день',
              'днем', 'с утра', 'числа', 'понедельник', 'вторник', 'среда',
              'среду', 'четверг', 'пятница', 'пятницу', 'суббота', 'субботу',
              'воскресенье', ' января', ' февраля', ' марта', ' июня', ' июля',
              ' августа', ' сентября', ' октября', ' ноября', ' декабря',
              ' день']


def have_date(request):
    for word in date_words:
        if word in request.lower():
            return True
    return any(char.isdigit() for char in request)


def start(bot, update):
    update.message.reply_text("Hi!")


def help(bot, update):
    update.message.reply_text(help_message)


def weather(bot, update):
    if (have_date(update.message.text)):
        try:
            responce, img_url = process_request(update.message.text)
            update.message.reply_text(responce)
            # urllib.request.urlretrieve(img_url, "img.png")
            # bot.send_photo(chat_id=update.message.chat_id, photo=open('img.png', 'rb'))
            try:
                bot.send_photo(chat_id=update.message.chat_id, photo=img_url)
            except Exception:
                pass
        except Exception:
            update.message.reply_text(wrong_request_message)
    else:
        try:
            process_detailed_request(update.message.text)
            bot.send_photo(chat_id=update.message.chat_id,
                           photo=open('plot.png', 'rb'))
        except Exception:
            update.message.reply_text(wrong_request_message)


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    updater = Updater('388685680:AAHG0rlS4z1K6MMY3GjumazGwFZOiml5LGY')

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    dp.add_handler(MessageHandler(Filters.text, weather))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()

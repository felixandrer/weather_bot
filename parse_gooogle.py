import requests
from bs4 import BeautifulSoup
from matplotlib import pyplot as plt
from apiclient.discovery import build


headers = {"User-Agent": ("Mozilla/5.0 (Windows NT 10.0; Win64; x64)" +
                          " AppleWebKit/537.36 (KHTML, like Gecko)" +
                          " Chrome/66.0.3359.139 Safari/537.36'}")}
google_url = "https://www.google.ru/search?q="
my_api_key = 'AIzaSyClnidvc6lv4ec5nV-Lzw2k1GPvLLw9euY'
my_cx = '015022844521674030376:v8-saul6wzi'


def get_celsium(temp1, temp2):
    if abs(9/5 * temp1 + 32 - temp2) < abs(9/5 * temp2 + 32 - temp1):
        return temp1
    else:
        return temp2


def get_weather(page):
    weather = {}
    soup = BeautifulSoup(page.text, "html5lib")
    weather['weather_description'] = soup.find(id="wob_dc").contents[0]
    temp1 = int(soup.find(id="wob_tm").contents[0])
    temp2 = int(soup.find(id="wob_ttm").contents[0])
    weather['temperature'] = str(get_celsium(temp1, temp2)) + 'C'
    weather['probability_of_precipitation'] = soup.find(id="wob_pp").contents[0]
    weather['humidity'] = soup.find(id="wob_hm").contents[0]
    weather['wind'] = soup.find(id="wob_tws").contents[0]
    weather['city'] = soup.find(id="wob_loc").contents[0]

    return weather


def assemble_responce(weather):
    response = ("{city}\n".format(**weather)
                + "Температура: {temperature}\n".format(**weather)
                + "{weather_description}\n".format(**weather)
                + "Вероятность осадков: {probability_of_precipitation}\n".format(**weather)
                + "Влажность: {humidity}\n".format(**weather)
                + "Ветер: {wind}\n".format(**weather))
    return response


def get_responce_and_img_url(weather):
    responce = assemble_responce(weather)
    image_description = 'фото+город+' + weather['city'].split(', ')[0]
    if int(weather['probability_of_precipitation'][:-1]) > 50:
        if int(weather['temperature'][:-1]) > 0:
            image_description += '+дождь'
        else:
            image_description += '+снег'
    # image_description = image_description + '+-карта'
    img_url = get_image_url(image_description)
    return responce, img_url


def get_image_url(image_description):
    service = build("customsearch", "v1", developerKey=my_api_key)

    res = service.cse().list(
        q=image_description,
        cx=my_cx,
        searchType='image',
        # imgSize='small',
        num=1,
        imgType='photo',
        safe='off'
    ).execute()

    if 'items' not in res:
        raise Exception
    return res['items'][0]['link']


def prepare_request_for_google(request):
    prepared = request.replace(' ', '+')
    if 'погод' not in request.lower():
        prepared = 'погода+' + prepared
    return prepared


def process_request(text):
    request = prepare_request_for_google(text)
    page = requests.get(google_url + request + '&hl=ru', headers=headers)
    with open('1.html', 'w') as f:
        f.write(page.text)
    weather = get_weather(page)
    responce, img_url = get_responce_and_img_url(weather)
    return responce, img_url


def get_detailed_weather(page):
    soup = BeautifulSoup(page.text, "html5lib")
    city = soup.find(id="wob_loc").contents[0]
    temps = [int(x.contents[0]) for x in soup.find_all('span',
             attrs={"class": "wob_t", "style": "display:none", "id": None})]
    weekdays = [x.contents[0] for x in soup.find_all('div',
                attrs={"class": "vk_lgy",
                       "style": "padding-top:7px;line-height:15px",
                       "id": None})]
    return temps, weekdays, city


def prepare_request_for_google(request):
    prepared = request.replace(' ', '+')
    if 'погод' not in request:
        prepared = 'погода+' + prepared
    return prepared


def process_detailed_request(text):
    request = prepare_request_for_google(text)
    page = requests.get(google_url + request + '&hl=ru&ie=UTF-8',
                        headers=headers)
    temps, weekdays, city = get_detailed_weather(page)
    plt.figure(figsize=(7, 4))
    plt.plot([temps[i] for i in range(0, len(temps), 2)], label='День',
             color='g')
    plt.plot([temps[i] for i in range(1, len(temps), 2)], label='Ночь',
             color='indigo')
    plt.xticks(range(len(weekdays)), weekdays)
    plt.legend()
    plt.title('Температура. {}'.format(city))
    plt.savefig('plot.png', bbox_inches='tight', dpi=150)
